package com.lblin.method.enums;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.lblin.method.body.BaseBody;

public enum MethodEnum {
	;

	private String target;//方法所在的目标类
	
	private String method;//方法名（相同类中不允许方法名重复，不同参数的方法名也请命名为不同的名称）
	
	private String desc;//方法职责
	
	private MethodEnum(String target,String method,String desc) {
		this.target = target;
		this.method = method;
		this.desc = desc;
	}
	
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	public Object doMethod(BaseBody body,Object ... args) {
		Object obj = body.getMyMethod().getMethodClass(this);
		Method m = body.getMyMethod().getMethod(this);
		Object result = null;
		try {
			result = m.invoke(obj, args);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
