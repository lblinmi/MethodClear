package com.lblin.method.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Monitor {
	
	String name() default "";//如果指定了name则在监控的时候则会以name的不同细分 关系域（仅对监控程序有用）
	
	String desc() default "";

}
