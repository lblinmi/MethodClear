package com.lblin.method.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.lblin.method.enums.MethodType;

@Monitor()
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Method {

	//MethodType type() default MethodType.PUBLIC;//PUBLIC--通用方法 ，PRIVATE--私有方法
	/**
	 * 使用者类
	 * @return
	 */
	Class<?>[] user();
	
	String desc() default ""; //方法描述
	
}
