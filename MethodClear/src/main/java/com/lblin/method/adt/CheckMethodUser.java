package com.lblin.method.adt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

import com.xiaoleilu.hutool.util.ObjectUtil;


public class CheckMethodUser {

	public static Map<String,Set<String>> methodUser = new HashMap<String,Set<String>>();//方法追踪链
	
	public static Map<Element,Set<String>> checkMethodIsUse(Set<Element> elements) {
		
		Map<Element,Set<String>> traces = new HashMap<Element,Set<String>>();
		for(Element ele : elements) {
			
			Set<String> methods = new HashSet<String>();
			ExecutableElement exele = (ExecutableElement)ele;
			/*获取类信息*/
            TypeElement typeElement = (TypeElement) exele.getEnclosingElement();
            /*全类名*/
            String absolute = typeElement.getQualifiedName().toString();
			String key = absolute+":"+exele.getSimpleName()+":"+exele.getParameters().size();
			
			traceTop(key,methods);
			
			//得到了当前@check的方法的顶层调用方法(@Method注解)集合
			if(methods.size()>0) {
				traces.put(ele, methods);
			}
		}
		
		return traces;
	}

	private static void traceTop(String key, Set<String> methods) {

		Set<String> values = methodUser.get(key);
		if(ObjectUtil.isNull(values)) {
			methods.add(key);
		}else {
			for(String v : values) {
				traceTop(v,methods);
			}
		}
		
	};
	
}
